
public class HomeTheaterFacede {
	private Tv tv;
	private BlurayPlayer brPlayer;
	
	public HomeTheaterFacede(Tv tv, BlurayPlayer brPlayer) {
		this.tv = tv;
		this.brPlayer = brPlayer;
	}
	
	public void watchMovie(String movie){
		System.out.println("Get ready to watch movie ...");
		tv.on();
		brPlayer.on();
	}
	
	public void endMovie(){
		tv.off();
		brPlayer.off();
	}
}
