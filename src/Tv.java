
public class Tv implements ElectricalDevice {

	@Override
	public void on() {
		System.out.println("Tv has turned on");

	}

	@Override
	public void off() {
		System.out.println("Tv has turned off");
	}

}
