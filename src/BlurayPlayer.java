
public class BlurayPlayer implements ElectricalDevice {

	@Override
	public void on() {
		System.out.println("Blu-ray player has turned on");
	}

	@Override
	public void off() {
		System.out.println("Blu-ray player has turned off");
	}

}
