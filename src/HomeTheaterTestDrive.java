
public class HomeTheaterTestDrive {
	
	public static void main(String[] args){
		Tv tv = new Tv();
		BlurayPlayer brPlayer = new BlurayPlayer();
		HomeTheaterFacede homeTheater = new HomeTheaterFacede(tv, brPlayer);
		
		homeTheater.watchMovie("The Matrix");
		
		homeTheater.endMovie();
	}
}
	

